from django.test import TestCase, Client
from django.urls import resolve
from .models import *
from .views import *

class Story6Test(TestCase):

    def test_kegiatan_page_url_is_exist(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    def test_kegiatan_page_using_kegiatan_function(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, kegiatan)

    def test_kegiatan_model(self):
        Kegiatan.objects.create(nama='TestKegiatan1')
        count = Kegiatan.objects.all().count()
        self.assertEqual(count, 1)
        name = str(Kegiatan.objects.get(id=1))
        self.assertEqual(name, 'TestKegiatan1')

    def test_peserta_model(self):
        Kegiatan.objects.create(nama='TestKegiatan1')
        DaftarPeserta.objects.create(peserta='John Doe',
                               kegiatan=Kegiatan.objects.get(id=1))
        count = DaftarPeserta.objects.all().count()
        self.assertEqual(count, 1)
        name = str(DaftarPeserta.objects.get(id=1))
        self.assertEqual(name, 'John Doe ikut TestKegiatan1')

    def test_peserta_page_is_not_none(self):
        self.assertIsNotNone(kegiatan)

    def test_peserta_page_is_using_correct_html(self):
        Kegiatan.objects.create(nama='TestKegiatan1')
        DaftarPeserta.objects.create(peserta='John Doe',
                               kegiatan=Kegiatan.objects.get(id=1))
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan.html')


