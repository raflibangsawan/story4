from django.shortcuts import render, redirect
import json
from django.core.serializers import serialize
from .models import Jadwal, MatkulForm

def formMatkul(request):
    
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('kuliah:formMatkul')

    else:
        form = MatkulForm()

    return render(request, 'forms.html', {'form': form})

def resultMatkul(request):

    data = Jadwal.objects.all()
    x_data = json.loads(serialize('json', data))

    return render(request, 'result.html', {'data':x_data})

def detailsMatkul(request, pkTarget):
    details = Jadwal.objects.filter(pk=pkTarget)
    detailsTarget = json.loads(serialize('json', details))

    return render(request, 'details.html', {'details':detailsTarget})

def deleteMatkul(request, pkTarget):
    target = Jadwal.objects.filter(pk=pkTarget)
    target.delete()

    x_data = json.loads(serialize('json', Jadwal.objects.all()))

    return render(request, 'result.html', {'data':x_data})
