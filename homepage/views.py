from django.shortcuts import render
import datetime

def index(request):
    return render(request, 'home.html')

def riwayat(request):
    return render(request, 'riwayat.html')

def waktu(request, diff = 0):
    return render(request, 'time.html', context={'value':datetime.datetime.now() + datetime.timedelta(hours=diff)})

# Create your views here.
