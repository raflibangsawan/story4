from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from .models import *

def kegiatan(request):
    daftarKegiatan = Kegiatan.objects.all()
    peserta = DaftarPeserta.objects.all()
    form = KegiatanForm(request.POST or None)
    if form.is_valid():
        form.save()
    return render(request, 'kegiatan.html', {'form': form, 'obj':daftarKegiatan, 'peserta':peserta})


# Create your views here.
