from django.db import models
from django.forms import ModelForm
from django.core.validators import MinValueValidator

class Jadwal(models.Model):
    nama = models.CharField(max_length=50)
    dosen = models.CharField(max_length=50)
    sks = models.IntegerField(validators=[MinValueValidator(0)])
    deskripsi = models.CharField(max_length=50)
    semester = models.CharField(max_length=20)
    kelas = models.CharField(max_length=10)

class Tugas(models.Model):
    tugas = models.CharField(max_length=50)
    deadline = models.DateField()

class MatkulForm(ModelForm):
    class Meta:
        model = Jadwal
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'semester', 'kelas']

