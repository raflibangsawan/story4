from django.urls import path
from . import views

app_name = 'kuliah'

urlpatterns = [
    path('add/', views.formMatkul, name='formMatkul'),
    path('result/', views.resultMatkul, name='resultMatkul'),
    path(r'^matkuldetails/(?P<pkTarget>)/$', views.detailsMatkul, name='detailsMatkul'),
    path(r'^result/(?P<pkTarget>)/$', views.deleteMatkul, name='deleteMatkul'),
]