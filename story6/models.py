from django.db import models
from django.forms import ModelForm
from django import forms

class Kegiatan(models.Model):
    nama = models.CharField(max_length=30)

    def __str__(self):
        return self.nama


class DaftarPeserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    peserta = models.CharField(max_length=50)

    def __str__(self):
        return self.peserta + " ikut " + self.kegiatan.nama

class KegiatanForm(ModelForm):
    class Meta:
        model = DaftarPeserta
        fields = '__all__'
        widgets = {
            'peserta': forms.TextInput(attrs={
                'class': 'form-control'
            }),
            'kegiatan': forms.Select(attrs={
                'class': 'form-control'
            }),
        }



# Create your models here.
